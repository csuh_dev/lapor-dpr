package com.panritech.fuad.laporkandepartment.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.model.UserItem
import com.panritech.fuad.laporkandepartment.view.ProgressBarView
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity(), ProgressBarView {
    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
        layout_form.visibility = View.INVISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
        layout_form.visibility = View.VISIBLE
    }

    private lateinit var auth: FirebaseAuth
    private lateinit var fireDatabase:FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private var departmentId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference

        hideProgressBar()

        btnLogin.onClick {
            validateForm()
        }

    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        validateUserType(currentUser)
    }

    private fun updateUI(user: FirebaseUser?) {
        Log.e("View", "Update View")
        if (user != null) {
            startActivity<MainActivity>("userUid" to user.uid)
            finish()
        }
        hideProgressBar()
    }

    private fun validateForm() {
        val userEmail = txt_user_email.text
        val userPassword = txt_user_password.text
        if (userEmail.isNotEmpty() && userPassword.isNotEmpty()) {
            showProgressBar()
            signIn("$userEmail", "$userPassword")
        } else {
            txt_login_status.setText(R.string.txt_form_declined)
        }
    }

    private fun signIn(email: String, password: String) {
        Log.d(TAG, "signIn : $email")
        showProgressBar()

        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        val user = auth.currentUser
                        validateUserType(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        snackbar(root_layout, "Maaf User Tidak Ditemukan")
                        updateUI(null)
                    }
                    // [START_EXCLUDE]
                    if (!task.isSuccessful) {
                        //txt_login_status.setText(R.string.txt_user_tidak_ditemukan)
                    }
                    // [END_EXCLUDE]
                }
    }

    private fun validateUserType(user: FirebaseUser?) {
        showProgressBar()
        Log.e("Sign Process", "search user $user")
        val usersListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val data = snapshot.getValue(UserItem::class.java)
                if (data?.type == "dpr" || data?.type == "skpd") {
                    departmentId = data.department
                    updateUI(user)
                } else {
                    snackbar(root_layout, "Maaf Anda Tidak Punya Akses")
                    FirebaseAuth.getInstance().signOut()
                    updateUI(null)
                }
                hideProgressBar()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("firebaseError", "${databaseError.toException()}")
            }
        }
        if (user == null) {
            updateUI(null)
        } else {
            myRef.child("users/${user.uid}").addListenerForSingleValueEvent(usersListener)
        }
    }

    companion object {
        private const val TAG = "EmailPassword"
    }
}

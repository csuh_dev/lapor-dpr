package com.panritech.fuad.laporkandepartment.activity

import android.net.Uri
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.fragment.ReportDescriptionFragment
import com.panritech.fuad.laporkandepartment.fragment.ReportFinishedFragment
import com.panritech.fuad.laporkandepartment.fragment.ReportProcessFragment

import kotlinx.android.synthetic.main.activity_report_detail.*
import org.jetbrains.anko.startActivity
import java.security.AlgorithmConstraints

class ReportDetailActivity : AppCompatActivity(), ReportFinishedFragment.OnFragmentInteractionListener
        ,ReportDescriptionFragment.OnFragmentInteractionListener
        ,ReportProcessFragment.OnFragmentInteractionListener{
    override fun onChangeReportDepartmentListener(isChange: Boolean) {
        if (isChange)
            finish()
    }

    override fun onAddFinishedInteractionListener(reportId: String) {
        startActivity<AddReportActivity>("reportId" to reportId
                , "reportStatus" to "finished", "button" to "finish")
    }

    override fun onAddProcessInteractionListener(reportId: String) {
        startActivity<AddReportActivity>("reportId" to reportId
                ,"reportStatus" to "proses", "button" to "process")
    }

    override fun onDiscussionInteractionListener(reportId: String?) {
        startActivity<DiskusiActivity>("reportId" to reportId)
    }

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var reportId: String = ""
    private var reportTitle: String = ""
    private var reportDescription: String = ""
    private var reportLocation: String = ""
    private var reportDepartment: String = ""
    private var reportDate: String = ""
    private var reportStatus: String = ""
    private var reporterId: String = ""
    private var reportImg: String = ""
    private var prosesDate: String = ""
    private var prosesDescription: String = ""
    private var prosesImg: String = ""
    private var finishedDate: String = ""
    private var finishedDescription: String = ""
    private var finishedImg: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_detail)
        supportActionBar?.elevation = 0F
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))

        reportId = intent.getStringExtra("reportId")
        reportTitle = intent.getStringExtra("reportTitle")
        reportDescription = intent.getStringExtra("reportDescription")
        reportLocation = intent.getStringExtra("reportLocation")
        reportDepartment = intent.getStringExtra("reportDepartment")
        reportDate = intent.getStringExtra("reportDate")
        reportStatus = intent.getStringExtra("reportStatus")
        reporterId = intent.getStringExtra("reporterId")
        reportImg = intent.getStringExtra("reportImg")
        prosesDate = intent.getStringExtra("prosesDate")
        prosesDescription = intent.getStringExtra("prosesDescription")
        prosesImg = intent.getStringExtra("prosesImg")
        finishedDate = intent.getStringExtra("finishedDate")
        finishedDescription = intent.getStringExtra("finishedDescription")
        finishedImg = intent.getStringExtra("finishedImg")
        title = "Detail Pengaduan"
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_report_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return when (item.itemId){
            android.R.id.home -> {
                finish()
                true
            }

            R.id.action_settings -> {
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return when(position){
                0 -> ReportDescriptionFragment.newInstance(reportId, reportTitle, reportDescription,
                        reportLocation, reportDepartment, reportDate, reporterId, reportImg)
                1 -> ReportProcessFragment.newInstance(reportId,prosesDate, prosesDescription, prosesImg)
                else -> ReportFinishedFragment.newInstance(reportId,finishedDate,finishedDescription,finishedImg)
            }
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 3
        }
    }
}

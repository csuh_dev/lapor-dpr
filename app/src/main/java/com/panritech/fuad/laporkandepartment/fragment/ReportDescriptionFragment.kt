package com.panritech.fuad.laporkandepartment.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.firebase.database.*
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.model.DepartmentItem
import com.panritech.fuad.laporkandepartment.model.ReportItem
import kotlinx.android.synthetic.main.fragment_report_description.*
import kotlinx.android.synthetic.main.fragment_report_description.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_REPORT_ID = "reportId"
private const val ARG_REPORT_TITLE = "reportTitle"
private const val ARG_REPORT_DESCRIPTION = "reportDescription"
private const val ARG_REPORT_LOCATION = "reportLocation"
private const val ARG_REPORT_DEPARTMENT = "reportDepartment"
private const val ARG_REPORT_DATE = "reportDate"
private const val ARG_REPORTER_ID = "reporterId"
private const val ARG_REPORT_IMG = "reportImg"

class ReportDescriptionFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var reportId: String? = null
    private var reportTitle: String? = null
    private var reportDescription: String? = null
    private var reportLocation: String? = null
    private var reportDepartment: String? = null
    private var reportDate: String? = null
    private var reporterId: String? = null
    private var reportImg: String? = null
    private var department: MutableList<DepartmentItem> = mutableListOf()
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var myRef: DatabaseReference
    private lateinit var listDepartment: List<String>
    private var departmentIdChoose = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            reportId = it.getString(ARG_REPORT_ID)
            reportTitle = it.getString(ARG_REPORT_TITLE)
            reportDescription = it.getString(ARG_REPORT_DESCRIPTION)
            reportDate = it.getString(ARG_REPORT_DATE)
            reportLocation = it.getString(ARG_REPORT_LOCATION)
            reportDepartment = it.getString(ARG_REPORT_DEPARTMENT)
            reporterId = it.getString(ARG_REPORTER_ID)
            reportImg = it.getString(ARG_REPORT_IMG)
        }
        myRef = FirebaseDatabase.getInstance().reference
        val dialog = indeterminateProgressDialog("Tunggu Sebentar")
        dialog.show()
        getDepartment()
        dialog.cancel()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_report_description, container, false)
        val txtReportTitle = view.txt_report_title
        val txtReporterName = view.txt_reporter_name
        val txtReportLocation = view.txt_report_location
        val txtDepartment = view.txt_department
        val txtReportDate = view.txt_report_date
        val txtReportDescription = view.txt_report_description
        val btnMoveReport = view.btnMoveReport
        val btnDiscussion = view.btnDiskusi
        val imgReport = view.imageView2

        txtReportTitle.text = reportTitle
        txtReporterName.text = reporterId
        txtReportLocation.text = reportLocation
        txtDepartment.text = reportDepartment
        txtReportDate.text = reportDate
        txtReportDescription.text = reportDescription
        if(reportImg!=""){
            Glide.with(context!!).load(reportImg).into(imgReport)
        }

        btnMoveReport.onClick {
            selector("Alihkan ke:", listDepartment) { _, i ->
                toast("${listDepartment[i]} Menjadi Penanggungjawab")
                txt_department.text = listDepartment[i]
                departmentIdChoose = department[i].key
                getReportDetail()
            }
        }
        btnDiscussion.onClick { _ ->
            listener?.onDiscussionInteractionListener(reportId)
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun getDepartment() {
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.mapNotNullTo(department) {
                    it.getValue<DepartmentItem>(DepartmentItem::class.java)
                }
                val items: ArrayList<String> = arrayListOf()
                for (item in department) {
                    items.add(item.name)
                }
                listDepartment = items
                Log.e("departmentList", "$department")
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        department.clear()
        myRef.child("department/skpd").addValueEventListener(departmentListListener)
    }

    private var reportItem: ReportItem? = null

    private fun getReportDetail() {
        val dialog = indeterminateProgressDialog("Mengalihkan Laporan")
        dialog.show()
        val reportDetail = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val item = snapshot.getValue(ReportItem::class.java)
                reportItem = item
                Log.e("item", reportItem.toString())
                reportItem?.reportDepartment = departmentIdChoose
                reportItem?.reportStatus = "process"
                myRef.child("report/$reportId").setValue(reportItem)
                Log.e("editData", "Berhasil Edit Data")
                dialog.cancel()
            }
        }
        myRef.child("report/$reportId").addListenerForSingleValueEvent(reportDetail)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onDiscussionInteractionListener(reportId: String?)
        fun onChangeReportDepartmentListener(isChange: Boolean)
    }

    companion object {

        @JvmStatic
        fun newInstance(reportId: String, reportTitle: String, reportDescription: String,
                        reportLocation: String, reportDepartment: String,
                        reportDate: String, reporterId: String, reportImg: String) =
                ReportDescriptionFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_REPORT_ID, reportId)
                        putString(ARG_REPORT_TITLE, reportTitle)
                        putString(ARG_REPORT_DESCRIPTION, reportDescription)
                        putString(ARG_REPORT_LOCATION, reportLocation)
                        putString(ARG_REPORT_DEPARTMENT, reportDepartment)
                        putString(ARG_REPORT_DATE, reportDate)
                        putString(ARG_REPORTER_ID, reporterId)
                        putString(ARG_REPORT_IMG, reportImg)
                    }
                    Log.e("newInstance : ", "$reportId - $reportTitle - $reportDescription - $reportDate")
                }
    }
}

package com.panritech.fuad.laporkandepartment.model

data class DepartmentItem (
        var key: String = "",
        var name: String = "",
        var description: String = ""
)
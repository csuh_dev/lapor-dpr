package com.panritech.fuad.laporkandepartment.view

import com.panritech.fuad.laporkandepartment.model.ReportItem

interface ReportView {
    fun showReportItem(data: List<ReportItem>)
}
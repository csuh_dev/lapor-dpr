package com.panritech.fuad.laporkandepartment.presenter

import com.panritech.fuad.laporkandepartment.model.AnggotaDprItem
import com.panritech.fuad.laporkandepartment.model.ReportItem
import com.panritech.fuad.laporkandepartment.model.UserItem
import com.panritech.fuad.laporkandepartment.view.AnggotaDprView
import com.panritech.fuad.laporkandepartment.view.ProgressBarView
import com.panritech.fuad.laporkandepartment.view.ReportView

class AnggotaDprPresenter (private val anggotaDprView: AnggotaDprView, private val progressBarView: ProgressBarView){

    fun getAnggotaDprList(data: List<UserItem>){
        progressBarView.showProgressBar()
        anggotaDprView.showAnggotaDprItem(data)
    }
}
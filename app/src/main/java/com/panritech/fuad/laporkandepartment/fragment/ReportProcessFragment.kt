package com.panritech.fuad.laporkandepartment.fragment

import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.model.ReportItem
import com.panritech.fuad.laporkandepartment.model.UserItem
import kotlinx.android.synthetic.main.fragment_report_process.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.indeterminateProgressDialog

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_REPORT_ID = "reportId"
private const val ARG_PROSES_DATE = "prosesDate"
private const val ARG_PROSES_DESCRIPTION = "prosesDescription"
private const val ARG_PROSES_IMG = "prosesImg"

class ReportProcessFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var reportId: String = ""
    private var prosesDate: String? = ""
    private var prosesDescription: String? = ""
    private var prosesImg: String? = ""
    private var userId: String? = ""
    private var userLevel: String? = ""
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var myRef: DatabaseReference
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            reportId = it.getString(ARG_REPORT_ID)
            prosesDate = it.getString(ARG_PROSES_DATE)
            prosesDescription = it.getString(ARG_PROSES_DESCRIPTION)
            prosesImg = it.getString(ARG_PROSES_IMG)
        }
        myRef = FirebaseDatabase.getInstance().reference
        auth = FirebaseAuth.getInstance()
        userId = auth.currentUser?.uid.toString()
        getUserName()
        Log.e("Report Data ","$reportId $prosesDate $prosesDescription")
        val dialog = indeterminateProgressDialog("Tunggu Sebentar")
        dialog.show()
        dialog.cancel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_report_process, container, false)
        val reportDescription = view.findViewById<TextView>(R.id.txt_report_process)
        val reportDate = view.findViewById<TextView>(R.id.txt_report_date)
        val btnAddProcess = view.findViewById<Button>(R.id.btnAddProgress)
        val reportImage = view.findViewById<ImageView>(R.id.img_report)

        if (prosesImg != "")
            Glide.with(this).load(prosesImg).into(reportImage)

        reportDescription.text = prosesDescription
        reportDate.text = prosesDate
        if (prosesDescription != "")
            btnAddProcess.text = "Edit Proses"

        btnAddProcess.onClick {
            listener?.onAddProcessInteractionListener(reportId)
        }
        return view
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun getUserName() {
        val userListListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadUser:onCancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(UserItem::class.java)
                Log.e("user", user.toString())
                if (user != null) {
                    userLevel = user.level
                }
                if(userLevel!="ketua"){
                    btnAddProgress.visibility = View.GONE
                }
            }
        }
        myRef.child("users/${userId}").addListenerForSingleValueEvent(userListListener)
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onAddProcessInteractionListener(reportId: String)
    }

    companion object {
        @JvmStatic
        fun newInstance(reportId: String, prosesDate: String, prosesDescription: String, prosesImg: String) =
                ReportProcessFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_REPORT_ID, reportId)
                        putString(ARG_PROSES_DATE, prosesDate)
                        putString(ARG_PROSES_DESCRIPTION, prosesDescription)
                        putString(ARG_PROSES_IMG, prosesImg)
                    }
                }
    }
}

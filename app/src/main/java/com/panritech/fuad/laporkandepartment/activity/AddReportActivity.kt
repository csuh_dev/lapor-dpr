package com.panritech.fuad.laporkandepartment.activity

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.model.ReportItem
import kotlinx.android.synthetic.main.activity_add_report.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import java.text.SimpleDateFormat
import java.util.*

class AddReportActivity : AppCompatActivity() {

    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_GALLERY = 2
    private lateinit var imageReport: ImageView
    private var fileUri: Uri? = null
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private var reportId: String = ""
    private var reportStatus: String = ""
    private var reportItem: ReportItem? = null
    private var button: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_report)
        imageReport = findViewById(R.id.imageReport)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        myRef = FirebaseDatabase.getInstance().reference

        reportStatus = intent.getStringExtra("reportStatus")
        reportId = intent.getStringExtra("reportId")
        button = intent.getStringExtra("button")

        getProcessDetail()

        textBtnDelete.onClick {
            imageReport.setImageBitmap(null)
        }

        textBtnGallery.onClick {
            takePictureGallery()
        }
        btnSendReport.onClick {
            dialogAddReport()
//            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getProcessDetail(){
        val getReportItem = object : ValueEventListener{
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("firebaseError", "${databaseError.toException()}")
            }
            override fun onDataChange(snapshot: DataSnapshot) {
                val reportDescription = snapshot.child("${reportStatus}Description").getValue(String::class.java)
                val reportDate = snapshot.child("${reportStatus}Date").getValue(String::class.java)
                val reportImg = snapshot.child("${reportStatus}Img").getValue(String::class.java)
                if (reportDescription != null) {
                    txtReportDescription.setText(reportDescription)
                }
                if(reportImg != "") {
                    Glide.with(this@AddReportActivity)
                            .load(reportImg)
                            .into(imageReport)
                }
            }
        }
        myRef.child("report/$reportId").addListenerForSingleValueEvent(getReportItem)
    }

    private fun dialogAddReport(){
        alert("Yakin Data Proses Yang Anda Masukkan Benar?") {
            yesButton {
                when (button){
                    "process" -> addReportProces()
                    else -> addReportFinish()
                }
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    private fun addReportProces(){
        val getReportItem = object : ValueEventListener{
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("firebaseError", "${databaseError.toException()}")
            }
            override fun onDataChange(snapshot: DataSnapshot) {

                val current = Calendar.getInstance().time
                val time = SimpleDateFormat("dd-MM-yyyy HH:mm").format(current)
                val item = snapshot.getValue(ReportItem::class.java)
                reportItem = item
                Log.e("item", reportItem.toString())
                reportItem?.prosesDescription = txtReportDescription.text.toString()
                reportItem?.prosesDate = time.toString()
                myRef.child("report/$reportId").setValue(reportItem)
                Log.e("item2", reportItem.toString())
                Log.e("editData", "Berhasil Edit Data")
                uploadImage(item?.reportId!!,"proses")
                finish()
            }
        }
        myRef.child("report/$reportId").addListenerForSingleValueEvent(getReportItem)
    }

    private fun uploadImage(key: String, status:String) {
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        if (fileUri != null) {
            val reference = storageRef.child("images/report/$key/$status")
            val uploadTask = reference.putFile(fileUri!!)
            uploadTask
                    .addOnFailureListener {
                        Log.e("Upload Error", it.toString())
                    }
                    .addOnSuccessListener {
                        Log.e("Upload Success", "Berhasil Upload Gambar")
                    }
                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>>{ task ->
                        if (!task.isSuccessful){
                            task.exception?.let {
                                throw it
                            }
                        }
                        return@Continuation reference.downloadUrl
                    })
                    .addOnCompleteListener {task ->
                        if (task.isSuccessful) {
                            val downloadUri = task.result
                            Log.e("Download Uri",downloadUri.toString())
                            myRef.child("report/$key/${status}Img").setValue(downloadUri.toString())
                        }else{
                            toast("Fail Get Uri")
                        }
                    }
        }
    }

    private fun addReportFinish(){
        val getReportItem = object : ValueEventListener{
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("firebaseError", "${databaseError.toException()}")
            }
            override fun onDataChange(snapshot: DataSnapshot) {

                val current = Calendar.getInstance().time
                val time = SimpleDateFormat("dd-MM-yyyy HH:mm").format(current)
                val item = snapshot.getValue(ReportItem::class.java)
                reportItem = item
                Log.e("item", reportItem.toString())
                reportItem?.finishedDescription = txtReportDescription.text.toString()
                reportItem?.finishedDate = time.toString()
                myRef.child("report/$reportId").setValue(reportItem)
                myRef.child("report/$reportId/reportStatus").setValue("finish")
                Log.e("item2", reportItem.toString())
                Log.e("editData", "Berhasil Edit Data")
                uploadImage(item?.reportId!!,"finished")
                finish()
            }
        }
        myRef.child("report/$reportId").addListenerForSingleValueEvent(getReportItem)
    }

    private fun cameraAlertDialog(){
        val dialogView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_camera, null)

        val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)

        // set message of alert dialog
        dialogBuilder.setMessage("Ambil Gambar Dengan Posisi HP Mendatar/Tidur")
                .setCancelable(false)
                .setPositiveButton("Ambil Gambar") { _, _ ->
                    dispatchTakePictureIntent()
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
        val alert = dialogBuilder.create()
        alert.setTitle("Perhatian")
        alert.show()
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun takePictureGallery(){
        val pickImageIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickImageIntent, REQUEST_IMAGE_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == RESULT_OK){
            when (requestCode){
                REQUEST_IMAGE_CAPTURE -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    imageReport.setImageBitmap(imageBitmap)
                }

                REQUEST_IMAGE_GALLERY -> {
                    fileUri = data?.data
                    Glide.with(this)
                            .load(fileUri)
                            .into(imageReport)
                }

                else -> {
                    super.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }
}

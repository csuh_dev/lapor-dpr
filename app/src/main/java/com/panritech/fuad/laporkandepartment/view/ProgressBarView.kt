package com.panritech.fuad.laporkandepartment.view

interface ProgressBarView {
    fun showProgressBar()
    fun hideProgressBar()
}
package com.panritech.fuad.laporkandepartment.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.adapter.MyDiskusiItemRecyclerViewAdapter
import com.panritech.fuad.laporkandepartment.model.CommentItem
import com.panritech.fuad.laporkandepartment.presenter.CommentPresenter
import com.panritech.fuad.laporkandepartment.view.DiskusiView
import kotlinx.android.synthetic.main.activity_diskusi.*

class DiskusiActivity : AppCompatActivity(), DiskusiView {

    private var comment: MutableList<CommentItem> = mutableListOf()
    private var listener: DiskusiActivity? = null
    private lateinit var commentPresenter: CommentPresenter
    private lateinit var adapter: MyDiskusiItemRecyclerViewAdapter


    override fun showCommentItem(data: List<CommentItem>) {
        addComment()
        adapter.notifyDataSetChanged()
    }

    private fun addComment(){
        var content = ""
        var time = ""
        for (i in 0..4) {
            val statusInt = i%3
            when(statusInt){
                0 -> {
                    content = "Progres sudah 20%"
                    time = "12.46"
                }
                1 -> {
                    content = "Komentar"
                    time = "12.46"
                }
                2 -> {
                    content = "Komentar Juga"
                    time = "12.46"
                }
            }
            val item = CommentItem(i.toString(), "Nama User", content,
                    time)
            comment.add(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diskusi)

        supportActionBar?.elevation = 0F
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Diskusi"

        reyclerview_message_list.layoutManager = LinearLayoutManager(this)
        Log.e("data", comment.toString())
        adapter = MyDiskusiItemRecyclerViewAdapter(comment, listener)
        reyclerview_message_list.adapter = adapter


        commentPresenter = CommentPresenter(this)
        commentPresenter.getCommentList(comment)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }

}

package com.panritech.fuad.laporkandepartment.view

import com.panritech.fuad.laporkandepartment.model.AnggotaDprItem
import com.panritech.fuad.laporkandepartment.model.DepartmentItem
import com.panritech.fuad.laporkandepartment.model.DinasItem
import com.panritech.fuad.laporkandepartment.model.ReportItem

interface DinasView {
    fun showDinasItem(data: List<DepartmentItem>)
}
package com.panritech.fuad.laporkandepartment.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.adapter.MyReportItemRecyclerViewAdapter
import com.panritech.fuad.laporkandepartment.model.DepartmentItem

import com.panritech.fuad.laporkandepartment.model.ReportItem
import com.panritech.fuad.laporkandepartment.model.UserItem
import com.panritech.fuad.laporkandepartment.presenter.ReportPresenter
import com.panritech.fuad.laporkandepartment.view.ProgressBarView
import com.panritech.fuad.laporkandepartment.view.ReportView

class ReportItemFragment : Fragment(), ReportView, ProgressBarView {
    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showReportItem(data: List<ReportItem>) {
        Log.e("ReportItemFragment:data", "$report")
        report.clear()
        report.addAll(data)
        filterBy(status)
        adapter.notifyDataSetChanged()
        hideProgressBar()
        swipeRefresh.isRefreshing = false
    }

    private var userDepartment = ""

    private var departmentList: HashMap<String, String> = hashMapOf()
    private var reportList: MutableList<ReportItem> = mutableListOf()
    private var report: MutableList<ReportItem> = mutableListOf()
    private var listener: OnListFragmentInteractionListener? = null
    private var status: String = ""
    private lateinit var auth: FirebaseAuth
    private lateinit var myRef: DatabaseReference
    private lateinit var reportPresenter: ReportPresenter
    private lateinit var adapter: MyReportItemRecyclerViewAdapter
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var statusSpinner: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        auth = FirebaseAuth.getInstance()
        myRef = FirebaseDatabase.getInstance().reference
        adapter = MyReportItemRecyclerViewAdapter(report, listener)
        reportPresenter = ReportPresenter(this,this)
        getDepartment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_reportitem, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list_report)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        progressBar = view.findViewById(R.id.progressBar)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        statusSpinner = view.findViewById(R.id.spinner_status)

        statusSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                status = parent?.getItemAtPosition(position).toString().toLowerCase()
                if (position == 0)
                    status = ""
                filterBy(status)
            }
        }

        swipeRefresh.setOnRefreshListener {
            getReportData()
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun filterBy(status: String){
        adapter.items = report.asSequence().filter{
            it.reportStatus!!.contains(status)
        }.toMutableList()
        adapter.notifyDataSetChanged()
    }

    private fun getReportData() {
        val reportEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                reportList.clear()
                for (item in snapshot.children){
                    val data = item.getValue(ReportItem::class.java)
                    if (data != null) {
                        if (departmentList[data.reportDepartment] == userDepartment)
                            reportList.add(data)
                    }
                }
                reportList.forEach {
                    if (it.reportDepartment != ""){
                        it.reportDepartment = departmentList[it.reportDepartment]
                    }
                }

                reportPresenter.getReportList(reportList)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.child("report").addValueEventListener(reportEventListener)
    }

    private fun getDepartment() {
        val userId = auth.currentUser?.uid
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val data: MutableList<DepartmentItem> = mutableListOf()
                snapshot.children.mapNotNullTo(data) {
                    it.getValue<DepartmentItem>(DepartmentItem::class.java)
                }
                for (item in data) {
                    departmentList[item.key] = item.name
                }
                Log.e("departmentList", "$data")
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        val userDepartmentListener = object :ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val data = snapshot.getValue(UserItem::class.java)
                if (data != null) {
                    userDepartment = data.department
                }
                getReportData()
            }

        }
        myRef.child("department/dpr").addValueEventListener(departmentListListener)
        myRef.child("department/skpd").addValueEventListener(departmentListListener)
        myRef.child("users/$userId").addListenerForSingleValueEvent(userDepartmentListener)
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: ReportItem)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                ReportItemFragment()
    }
}

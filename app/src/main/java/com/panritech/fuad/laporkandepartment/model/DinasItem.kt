package com.panritech.fuad.laporkandepartment.model

data class DinasItem (
        var dinasId: String? = "",
        var dinasName: String? = "",
        var dinasPeople: String? = ""
)
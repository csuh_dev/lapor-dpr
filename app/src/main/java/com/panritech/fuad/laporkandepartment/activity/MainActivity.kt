package com.panritech.fuad.laporkandepartment.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.fragment.*
import com.panritech.fuad.laporkandepartment.model.ReportItem
import com.panritech.fuad.laporkandepartment.model.UserItem
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity(),
        ReportItemFragment.OnListFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener{

    override fun onLogoutInteraction(logout: Boolean) {
        if (logout){
            FirebaseAuth.getInstance().signOut()
            startActivity<LoginActivity>()
            finish()
        }
    }

    override fun onListFragmentInteraction(item: ReportItem) {
        getUserName(item)
    }

    private fun getUserName(item: ReportItem) {
        val dialog = indeterminateProgressDialog("Mengambil Data")
        dialog.show()
        myRef = FirebaseDatabase.getInstance().reference
        val userListListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadUser:onCancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(UserItem::class.java)
                Log.e("Value", user.toString())
                if (user != null) {
                    userName = user.name
                }
                dialog.cancel()
                startActivityDetail(item)
            }
        }
        Log.e("ReporterId", item.reporterId)
        myRef.child("users/${item.reporterId}").addListenerForSingleValueEvent(userListListener)
    }

    private fun startActivityDetail(item: ReportItem) {
        startActivity<ReportDetailActivity>("reportId" to item.reportId,
                "reportTitle" to item.reportTitle,
                "reportDescription" to item.reportDescription,
                "reportLocation" to item.reportLocation,
                "reportDepartment" to item.reportDepartment,
                "reportDate" to item.reportDate,
                "reportStatus" to item.reportStatus,
                "reporterId" to userName,
                "reportImg" to item.reportImg,
                "prosesDate" to item.prosesDate,
                "prosesDescription" to item.prosesDescription,
                "prosesImg" to item.prosesImg,
                "finishedDate" to item.finishedDate,
                "finishedDescription" to item.finishedDescription,
                "finishedImg" to item.finishedImg)
        userName = ""
    }

    private var userName: String = ""

    private lateinit var myRef: DatabaseReference

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_report -> {
                title = "Pengaduan"
                val fragment = ReportItemFragment.newInstance()
                openFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_info -> {
                title = "Info"
                val fragment = InfoFragment.newInstance()
                openFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                title = "Profil"
                val fragment = ProfileFragment.newInstance("")
                openFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            replace(R.id.content, fragment)
            commit()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.elevation = 0F

        val fragment = ReportItemFragment.newInstance()
        openFragment(fragment)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
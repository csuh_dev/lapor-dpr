package com.panritech.fuad.laporkandepartment.presenter

import com.panritech.fuad.laporkandepartment.model.ReportItem
import com.panritech.fuad.laporkandepartment.view.ProgressBarView
import com.panritech.fuad.laporkandepartment.view.ReportView

class ReportPresenter (private val reportView: ReportView, private val progressBarView: ProgressBarView){

    fun getReportList(data: List<ReportItem>){
        progressBarView.showProgressBar()
        reportView.showReportItem(data)
    }
}
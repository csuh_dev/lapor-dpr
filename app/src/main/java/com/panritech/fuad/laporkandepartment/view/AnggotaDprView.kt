package com.panritech.fuad.laporkandepartment.view


import com.panritech.fuad.laporkandepartment.model.UserItem

interface AnggotaDprView {
    fun showAnggotaDprItem(data: List<UserItem>)
}
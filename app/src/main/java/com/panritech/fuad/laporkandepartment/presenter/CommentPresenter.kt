package com.panritech.fuad.laporkandepartment.presenter

import com.panritech.fuad.laporkandepartment.model.CommentItem
import com.panritech.fuad.laporkandepartment.view.DiskusiView

class CommentPresenter (private val diskusiView: DiskusiView){

    fun getCommentList(data: List<CommentItem>){
        diskusiView.showCommentItem(data)
    }
}
package com.panritech.fuad.laporkandepartment.model

data class CommentItem (
        var commentId: String? = "",
        var commentUser: String? = "",
        var commentContent: String? = "",
        var commentTime: String? = ""
)
package com.panritech.fuad.laporkandepartment.presenter

import com.panritech.fuad.laporkandepartment.model.DepartmentItem
import com.panritech.fuad.laporkandepartment.model.DinasItem
import com.panritech.fuad.laporkandepartment.view.DinasView
import com.panritech.fuad.laporkandepartment.view.ProgressBarView

class DinasPresenter (private val dinasView: DinasView, private val progressBarView: ProgressBarView){

    fun getDinasList(data: List<DepartmentItem>){
        progressBarView.showProgressBar()
        dinasView.showDinasItem(data)
        progressBarView.hideProgressBar()
    }
}
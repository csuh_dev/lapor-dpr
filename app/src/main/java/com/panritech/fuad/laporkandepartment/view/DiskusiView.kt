package com.panritech.fuad.laporkandepartment.view

import com.panritech.fuad.laporkandepartment.model.AnggotaDprItem
import com.panritech.fuad.laporkandepartment.model.CommentItem
import com.panritech.fuad.laporkandepartment.model.DinasItem
import com.panritech.fuad.laporkandepartment.model.ReportItem

interface DiskusiView {
    fun showCommentItem(data: List<CommentItem>)
}
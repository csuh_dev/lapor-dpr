package com.panritech.fuad.laporkandepartment.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.panritech.fuad.laporkandepartment.R
import com.panritech.fuad.laporkandepartment.activity.AnggotaDprItemActivity
import com.panritech.fuad.laporkandepartment.activity.DiskusiActivity
import com.panritech.fuad.laporkandepartment.fragment.ReportItemFragment
import com.panritech.fuad.laporkandepartment.model.AnggotaDprItem
import com.panritech.fuad.laporkandepartment.model.CommentItem
import org.jetbrains.anko.find

class MyDiskusiItemRecyclerViewAdapter(
        var items: MutableList<CommentItem>,
        private val mListener: DiskusiActivity?)
    : RecyclerView.Adapter<MyDiskusiItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_diskusi_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val txtUser:TextView = view.find(R.id.txtName)
        val txtCommentContent:TextView = view.find(R.id.txtComment)
        val txtTime:TextView = view.find(R.id.txtCommentDate)

        fun bindItem(items: CommentItem){
            txtUser.text = items.commentUser
            txtCommentContent.text = items.commentContent
            txtTime.text = items.commentTime
        }
    }
}